<!DOCTYPE html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=yes">
    <meta charset="utf-8" />
    <link rel="shortcut icon" href="<?php bloginfo('template_url'); ?>/dest/images/favicon.ico" type="image/x-icon">
</head>
<body <?php body_class(); ?>>
<main id="main">
    
    