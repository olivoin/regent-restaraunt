<?php get_header(); ?>
<section class="full-height dis-flex align-items-center">
    <div class="section-wrapper dis-flex flex-wrap-wrap justify-content-center full-height">
        <div class="col-lg-vw-8 col-lm-9 col-xs-11 dis-flex justify-content-center full-height">
            <div class="dis-flex flex-wrap-wrap align-items-center align-content-center col-lg-8 col-xs-12 col-lm-12 full-height">
                <div class="col-lg-6 col-xs-12 col-lm-12 padding-r-30">
                    <img src="<?php bloginfo('template_url'); ?>/dest/images/404.svg">
                </div>
                <div class="col-lg-6 col-xs-12 col-lm-12 padding-l-30">
                    <div class="section-title type-1 margin-b-30">
                        <p>We lost this page in
                        one of our fuel
                        tanks.</p>
                    </div>
                    <a href="/" class="button">go home</a>
                </div>
            </div>
        </div>
    </div>
</section>
<?php get_footer(); ?>