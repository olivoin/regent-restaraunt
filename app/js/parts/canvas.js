jQuery(document).ready(function ($) {

    
    
    var canvas = document.getElementById('main-canvas');
    var main_context = canvas.getContext('2d');


    var total_scale = 0.75; //Масштаб  размеров существующих объектов, связанных с центральным полигоном и боковыми


    var startAngle = -10 * Math.PI / 180; //Начальный угол, то есть угол связующей линии самого первого полигона
    var radius = 307 * total_scale; //Длина связующих линий
    var x0 = 0.5; //Относительная величина x центра всех полигонов (вычисляется в зависимости от ширины канваса)
    var y0 = 0.4; //Относительная величина y центра всех полигонов (вычисляется в зависимости от высоты канваса)

    //Параметры текста слева от полигонов, где он высвечивается при активации / клике на полигон
    var mainText = {
        relX: 0.15,
        relY: 0.2,
        fontHeight: 16,
        relStringInterval: 0.2
    }

    var polygonsWidth = 220 * total_scale; //ширина полигона, отводимая для прорисовки каждого полигона в зависимости от его относительных координат
    var polygonsHeight = 220 * total_scale; //высота полигона, отводимая для прорисовки каждого полигона в зависимости от его относительных координат

    var activePolygon = null;

    //Прорисовка центрального полигона
    var polygonDraw = function (canvas, context, relCenterX, relCenterY, relRadius, sideCount, startAngle, fillColor) {
        var width = mainCanvasWidth;
        var height = mainCanvasHeight;




        var x0 = relCenterX * width;
        var y0 = relCenterY * height;

        var definitor = width;
        if (height < definitor) {
            definitor = height;
        }

        var radius = relRadius * definitor;

        var deltaAngle = 2 * Math.PI / sideCount;

        var currAngle = startAngle;

        context.beginPath();

        var startX = null;
        var startY = null;

        for (var index = 0; index < sideCount; index++) {

            currAngle = startAngle + deltaAngle * index;

            if (index === 0) {
                startX = x0 + radius * Math.cos(currAngle);
                startY = y0 + radius * Math.sin(currAngle);
                context.moveTo(startX, startY);
            } else {
                context.lineTo(x0 + radius * Math.cos(currAngle), y0 + radius * Math.sin(currAngle));
            }

        }

        if (startX !== null && startY !== null) {
            context.lineTo(startX, startY);
        }

        context.fillStyle = fillColor;

        context.fill();

        context.closePath();

    };

    var main_polygon_heart = new Image();

    main_polygon_heart.src = '../images/main-polygon-heart.png';

    var image_loaded = false;

    main_polygon_heart.onload = function () {
        image_loaded = true;
    }

    //Прорисовка картинки внутри полигона
    var drawImageOnPolygon = function (canvas, context, relX, relY, relWidth, relHeight) {
        var width = mainCanvasWidth;
        var height = mainCanvasHeight;

        var x0 = relX * width;
        var y0 = relY * height;

        var width = relWidth * width;
        var height = relHeight * height;

        if (image_loaded) {
            context.drawImage(main_polygon_heart, x0, y0, width, height);
        }
    }

    //Событие поднятия кнопки мыши при кликании на полигон (после отпускания его)
    var mouseUpFunction = function (object, event) {

        $.each(objects, function (i, obj) {
            if (obj !== object && obj.isActive()) {

                obj.setActivity(false);
            }
        });

    }


    //Функция прорисовки главного текущего текста слева от полигонов
    //Также её можно рассматривать как реакцию на активацию полигона (кликании на полигон)
    var onActivateFunction = function (object) {

        activePolygon = object;
        //var ctx = object.context;

        var mainTextStrings = object.informObject;


        $('#polygon-content-area').html(mainTextStrings);

        /*
        var size = mainText.fontHeight;
        var lineInterval = size * mainText.relStringInterval;
        var txtStringsLength = mainTextStrings.length;
        var totalTextHeight = (size + lineInterval) * txtStringsLength;
                    
                    
                    
        var textX0 = mainCanvasWidth * mainText.relX;
        var textY0 = mainCanvasHeight * mainText.relY;

        ctx.fillStyle = "red";
        ctx.font = "bold " + mainText.fontHeight + "px Arial";
                    
                    
        var textY;
                    
        for(var textIndex = 0; textIndex < txtStringsLength; textIndex++){
                textY = textY0 + (size + lineInterval) * textIndex + size;
                ctx.fillText(mainTextStrings[textIndex], textX0, textY);
        }
        */

    }

    //Входная информация по всем полигонам
    var polygons = [
        {
            polygon: [{
                x: 0,
                y: 1 - 0.8
            }, {
                x: 0.35,
                y: 1 - 1
            }, {
                x: 1,
                y: 1 - 0.5
            }, {
                x: 0.92,
                y: 1 - 0.08
            }, {
                x: 0.48,
                y: 1 - 0
            }, {
                x: 0.4,
                y: 1 - 0.45
            }], //Вершины полигона в относительной форме
            innerTextStrings: ['Distributors'], //Текст внутри полигона (список строк)
            mainTextStrings: '<div class="section-title type-1"><span>Distributors</span></div><div class="section-title type-2"><span>We offet competitive fasoil price that is <strong>40% lower</strong> than the current market</span></div>', //Главный текст слева от полигонов при активизации полигона (кликании по нему)
            relTextWidth: 1, //Относительная величина ширины текста внутри полигона, заданная для определения позиции по оси x внутри полигона. Не вычисляется. Задаётся вручную
            relTextDxFromCenter: -0.05,
            relTextDyFromCenter: -0.1,
                    },
        {
            polygon: [{
                x: 0,
                y: 1 - 1
            }, {
                x: 1,
                y: 1 - 0.45
            }, {
                x: 0.35,
                y: 1 - 0
            }],
            innerTextStrings: ['string1', 'string2', 'string3'],
            mainTextStrings: '<div class="section-title type-1 show"><span>Second heading</span></div><div class="section-title type-2 show"><span>Some description for second heading</span></div>', //Главный текст слева от полигонов при активизации полигона (кликании по нему)
            relTextWidth: 1,
            relTextDxFromCenter: -0.25,
            relTextDyFromCenter: -0.1
                    },
        {
            polygon: [{
                x: 0,
                y: 1 - 0.6
            }, {
                x: 0.16,
                y: 1 - 1
            }, {
                x: 0.6,
                y: 1 - 0.8
            }, {
                x: 1,
                y: 1 - 0.46
            }, {
                x: 0.7,
                y: 1 - 0.085
            }, {
                x: 0.21,
                y: 1 - 0
            }],
            innerTextStrings: ['string1', 'string2', 'string3'],
            mainTextStrings: '<div class="section-title type-1 show"><span>Third heading</span></div><div class="section-title type-2 show"><span>Some description for third heading</span></div>', //Главный текст слева от полигонов при активизации полигона (кликании по нему)
            relTextWidth: 1,
            relTextDxFromCenter: -0.25,
            relTextDyFromCenter: -0.1
                    },
        {
            polygon: [{
                x: 0.1,
                y: 1 - 0.02
            }, {
                x: 0.13,
                y: 1 - 1
            }, {
                x: 0.65,
                y: 1 - 0.7
            }, {
                x: 0.95,
                y: 1 - 0
            }],
            innerTextStrings: ['string1', 'string2', 'string3'],
            mainTextStrings: '<div class="section-title type-1 show"><span>Fourth heading</span></div><div class="section-title type-2 show"><span>Some description for fourth heading</span></div>', //Главный текст слева от полигонов при активизации полигона (кликании по нему)
            relTextWidth: 1,
            relTextDxFromCenter: -0.3,
            relTextDyFromCenter: -0.1
                    },
        {
            polygon: [{
                x: 0,
                y: 1 - 0.35
            }, {
                x: 0.65,
                y: 1 - 1
            }, {
                x: 1,
                y: 1 - 0.8
            }, {
                x: 0.4,
                y: 1 - 0
            }],
            innerTextStrings: ['string1', 'string2', 'string3'],
            mainTextStrings: '<div class="section-title type-1 show"><span>Thinkth heading</span></div><div class="section-title type-2 show"><span>Some description for thinkth heading</span></div>', //Главный текст слева от полигонов при активизации полигона (кликании по нему)
            relTextWidth: 1,
            relTextDxFromCenter: -0.25,
            relTextDyFromCenter: -0.1
                    },
        {
            polygon: [{
                x: 0,
                y: 1 - 0.6
            }, {
                x: 1,
                y: 1 - 0.8
            }, {
                x: 0.8,
                y: 1 - 0.1
            }, {
                x: 0.2,
                y: 1 - 0.2
            }],
            innerTextStrings: ['string1', 'string2', 'string3'],
            mainTextStrings: '<div class="section-title type-1 show"><span>Sexth heading</span></div><div class="section-title type-2 show"><span>Some description for sexth heading</span></div>', //Главный текст слева от полигонов при активизации полигона (кликании по нему)
            relTextWidth: 1,
            relTextDxFromCenter: -0.25,
            relTextDyFromCenter: -0.25
                    },
        {
            polygon: [{
                x: 0,
                y: 1 - 0.64
            }, {
                x: 0.89,
                y: 1 - 0.9
            }, {
                x: 1,
                y: 1 - 0.27
            }, {
                x: 0.2,
                y: 1 - 0.1
            }],
            innerTextStrings: ['string1', 'string2', 'string3'],
            mainTextStrings: '<div class="section-title type-1 show"><span>Seventh heading</span></div><div class="section-title type-2 show"><span>Some description for seventh heading</span></div>', //Главный текст слева от полигонов при активизации полигона (кликании по нему)
            relTextWidth: 1,
            relTextDxFromCenter: -0.25,
            relTextDyFromCenter: -0.25
                    }
                ];



    var angles = [];

    var deltaAngle = 2 * Math.PI / polygons.length;

    for (var index = 0; index < polygons.length; index++) {
        angles.push(startAngle + deltaAngle * index);
    }

    var objects = [];



    for (var index = 0; index < polygons.length; index++) {
        objects.push(
            new Particle({
                polygon: polygons[index].polygon,
                canvas: canvas,
                context: main_context,
                angle: angles[index],
                //standartAngle: 0,
                radius: radius,
                //standartRadius: 250,
                radiusDecreaseKoeff: 0.22,
                angleDecreaseKoeff: 0.2,
                width: polygonsWidth,
                height: polygonsHeight,
                angleFps: 24,
                radiusFps: 24,
                displayFps: 36,
                radiusKoeff: 0.22,
                x0: x0,
                y0: y0,

                mouseUpFunction: mouseUpFunction,

                textDisplayFunction: function (object, ctx, startX, startY) {


                    var size = object.textHeight;
                    var lineInterval = size * 0.25;
                    var txtStringsLength = object.textStrings.length;
                    var totalTextHeight = (size + lineInterval) * txtStringsLength;

                    var totalTextWidth = object.textBox.width;

                    //var offsetX = -totalTextWidth / 2;
                    //var offsetY = -totalTextHeight / 2;

                    var dx = object.textBox.relTextDxFromCenter * object.width;
                    var dy = object.textBox.relTextDyFromCenter * object.height;

                    //console.log(object.textBox.relTextDxFromCenter);

                    var textX0 = startX + dx;
                    var textY0 = startY + dy;

                    ctx.fillStyle = "white";
                    ctx.font = "italic " + object.textHeight + "px Arial";


                    var textY;

                    for (var textIndex = 0; textIndex < txtStringsLength; textIndex++) {
                        textY = textY0 + (size + lineInterval) * textIndex + size;
                        ctx.fillText(object.textStrings[textIndex], textX0, textY);
                    }


                },

                relativeStartTextX: 0.5,
                relativeStartTextY: 0.5,

                textBox: {
                    width: polygonsWidth * polygons[index].relTextWidth,
                    relTextDxFromCenter: polygons[index].relTextDxFromCenter,
                    relTextDyFromCenter: polygons[index].relTextDyFromCenter
                },

                textHeight: 16 * total_scale,

                textStrings: polygons[index].innerTextStrings,

                onActivateFunction: onActivateFunction,

                informObject: polygons[index].mainTextStrings,

                redrawMode: false
            })
        );
    }

    objects[0].setActivity(true);

    var mainCanvasWidth = $(canvas).width();
    var mainCanvasHeight = $(canvas).height();

    var relDrawWidth = 0.05 * total_scale;
    var relDrawHeight = 0.14 * total_scale;

    var imageX0 = x0 - relDrawWidth / 2;
    var imageY0 = y0 - relDrawHeight / 2;

    var redrawTimer = setInterval(function () {



        main_context.clearRect(0, 0, mainCanvasWidth, mainCanvasHeight);

        //if(activePolygon !== null){
        //activePolygon.onActivateFunction(activePolygon);
        //}







        for (var index = 0; index < objects.length; index++) {
            objects[index].draw();
        }

        polygonDraw(canvas, main_context, x0, y0, 0.11 * total_scale, 7, 0, '#312969');

        drawImageOnPolygon(canvas, main_context, imageX0, imageY0, relDrawWidth, relDrawHeight);


    }, 1000 / 24);







});