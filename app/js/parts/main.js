jQuery(document).ready(function ($) {

    // global 

    $(".tel").mask("+7 (999) 999-9999");

    new WOW().init();

    $('select').niceSelect();

    transformicons.add('.tcon');

    // header 

    if ($(window).width() < 1100) {
        $('#header').append($('.language-switch'));
        $('#nav .widget_nav_menu').append($('.header-bottom'));
        //$('.section-one .section-wrapper').append($('.section-wrapper-slider_desc'));
        $('#nav ul li a').click(function () {
            $('#nav .widget_nav_menu').removeClass('opened');
            $('.tcon').removeClass('tcon-transform');
        });
    }
    
//    if ($(window).width() == 768 && $(window).width() <= 1024) {
//        
//    }
    
    var mql = window.matchMedia('all and (min-width: 768px) and (max-width: 1024px)');
    if (mql.matches) {
        $('.section-one .section-wrapper').append($('.section-wrapper-slider_desc'));
    }

    $('.header-burger').click(function () {
        $('#nav .widget_nav_menu').toggleClass('opened');
    });

    // front page

    $('#tabs').multitabs();
    // footer

    $('.modal-close,.callback-btn').click(function () {
        $('.modal-overlay').toggleClass('opened');
    });

    // front page

    var $status = $('.pagingInfo');
    var $slickElement = $('.section-slider');

    $slickElement.on('init reInit afterChange', function (event, slick, currentSlide, nextSlide) {
        //currentSlide is undefined on init -- set it to 0 in this case (currentSlide is 0 based)
        var i = (currentSlide ? currentSlide : 0) + 1;
        var slideText = $('.slick-current .section-one-slider-desc').text();
        var slideImage = $('.slick-current .section-one-slider-image img').attr('src');

        $status.text(i + '/' + slick.slideCount);
        $('.section-wrapper-slider_desc p').text(slideText);
        $('.section-one').css({
            'background-image':'url('+ slideImage +')' 
        });
    });

    $slickElement.slick({
        //variableWidth: true,
        //autoplay: true,
        //slidesToScroll: 1,
        mobileFirst: true,
        fade: true,
        dots: false,
        speed: 10,
        appendArrows: '.section-wrapper-slider-arrow'
    })

    // section four

    $('.section-four-parts-item:first').addClass('active');

    var activePart = $('.section-four-parts-item.active');
    var activePartTitle = activePart.find('.section-four-parts-title').text();
    var activePartDesc = activePart.find('.section-four-parts-desc').html();

    setTimeout(function () {
        $('.section-four').find('.section-title').removeClass('show').addClass('hide');
    }, 10);
    setTimeout(function () {
        $('.section-four-desc .type-3 span').text(activePartTitle);
        $('.section-four-desc .type-1 span').html(activePartDesc);
    }, 300);
    setTimeout(function () {
        $('.section-four').find('.section-title').removeClass('hide').addClass('show');
    }, 500);

    $('.section-four-parts-item').hover(
        function () {
            $(this).addClass('active');
            var activePart = $('.section-four-parts-item.active');
            var activePartTitle = activePart.find('.section-four-parts-title').text();
            var activePartDesc = activePart.find('.section-four-parts-desc').html();

            console.log(activePartTitle, activePartDesc);

            setTimeout(function () {
                $('.section-four').find('.section-title').removeClass('show').addClass('hide');
            }, 10);
            setTimeout(function () {
                $('.section-four-desc .type-3 span').text(activePartTitle);
                $('.section-four-desc .type-1 span').html(activePartDesc);
            }, 300);
            setTimeout(function () {
                $('.section-four').find('.section-title').removeClass('hide').addClass('show');
            }, 500);

        },
        function () {
            $(this).removeClass('active');
        }
    );

    //$('.section-four-parts-item:first .section-four-parts-title').click();

    // svg

    jQuery('img.svg').each(function () {
        var $img = jQuery(this);
        var imgID = $img.attr('id');
        var imgClass = $img.attr('class');
        var imgURL = $img.attr('src');

        jQuery.get(imgURL, function (data) {
            // Get the SVG tag, ignore the rest
            var $svg = jQuery(data).find('svg');

            // Add replaced image's ID to the new SVG
            if (typeof imgID !== 'undefined') {
                $svg = $svg.attr('id', imgID);
            }
            // Add replaced image's classes to the new SVG
            if (typeof imgClass !== 'undefined') {
                $svg = $svg.attr('class', imgClass + ' replaced-svg');
            }

            // Remove any invalid XML tags as per http://validator.w3.org
            $svg = $svg.removeAttr('xmlns:a');

            // Replace image with new SVG
            $img.replaceWith($svg);

        }, 'xml');

    });

});