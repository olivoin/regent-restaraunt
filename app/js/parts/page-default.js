jQuery(document).ready(function ($) {
    
    $("#tocList a").click(function (e) {
        e.preventDefault();
        var a = $("[data-anchor='" + $(this).attr("href").replace("#", "") + "']");
        $("html, body").animate({
            scrollTop: (a.offset().top - 50) + 'px'
        });
    });
    
    var mql = window.matchMedia('all and (min-width: 1024px)');
    if (mql.matches) {
        $(function () {
            return $("[data-sticky_column]").stick_in_parent({
                parent: "[data-sticky_parent]"
            });
        });
    }

    $('.page-privacy_right .full-width').append($('#toc_container'));
    
});