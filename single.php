<?php get_header(); ?>
<?php if(have_posts()) : ?>
<?php while(have_posts()) : the_post(); ?>
<section class="dis-flex justify-content-center">
    <div class="col-lg-8 col-md-10 col-xs-11 dis-flex flex-wrap-wrap">
        <div class="full-width dis-flex align-items-center margin-b-50">
            <a id="back-link" href="#" class="link-with_icon back margin-r-20">назад</a>
            <span class="single-post-date">
                <?php the_date(); ?>
            </span>
        </div>
        <div class="col-lg-vw-6 col-md-vw-7 col-xs-12">
            <div class="section-title mid">
                <h2><?php the_title(); ?></h2>
            </div>
            <div class="margin-t-50 text-block">
                <?php the_content(); ?>
            </div>
        </div>
        <div class="col-lg-vw-6">
            
        </div>
    </div>
</section>
<?php endwhile; ?>
<?php endif; ?>
<?php get_footer(); ?>