<?php

/* очищаем wp_head(); */
function remove_recent_comments_style() {  
  global $wp_widget_factory;  
  remove_action( 'wp_head', array( $wp_widget_factory->widgets['WP_Widget_Recent_Comments'], 'recent_comments_style' ) );  
}  
add_action( 'widgets_init', 'remove_recent_comments_style' );  
remove_action( 'wp_head', 'feed_links_extra', 3 ); 
remove_action( 'wp_head', 'feed_links', 2 );
remove_action( 'wp_head', 'rsd_link' );
remove_action( 'wp_head', 'wlwmanifest_link' );
remove_action( 'wp_head', 'index_rel_link' );
remove_action( 'wp_head', 'parent_post_rel_link', 10, 0 ); 
remove_action( 'wp_head', 'start_post_rel_link', 10, 0 );
remove_action( 'wp_head', 'adjacent_posts_rel_link', 10, 0 );
remove_action( 'wp_head', 'wp_generator' );


if(function_exists('register_sidebar'))
	register_sidebar(array('name' => 'Sidebar' )); 
	

if ( function_exists( 'add_theme_support' ) ) { 
  add_theme_support( 'post-thumbnails' );
}

// PHONE FIELD CORRECT

function save_book_meta( $post_id, $post, $update ) {

    $post_title = get_the_title( $post_id );
    $number =  preg_replace("/[^0-9]/", '', $post_title);
    if ( ! empty( $number )  )  {
            update_post_meta( $post_id, '_number_flug', $number  );
    }
 
}
add_action( 'save_post', 'save_book_meta', 10, 3 );

// settings site

/**
 * ACF Options Page
 */

if ( function_exists( 'acf_add_options_page' ) ) {

  
  // Main Theme Settings Page
  $parent = acf_add_options_page( array(
    'page_title' => 'Theme General Settings',
    'menu_title' => 'Theme Settings',
    'redirect'   => 'Theme Settings',
  ) );

  // 
  // Global Options
  // Same options on all languages. e.g., social profiles links
  // 

  acf_add_options_sub_page( array(
    'page_title' => 'Global Options',
    'menu_title' => __('Global Options', 'text-domain'),
    'menu_slug'  => "acf-options",
    'parent'     => $parent['menu_slug']
  ) );

  // 
  // Language Specific Options
  // Translatable options specific languages. e.g., social profiles links
  // 
  

}

function change_protected_title_prefix() {
    return '%s';
}
add_filter('protected_title_format', 'change_protected_title_prefix');

// on send ok

add_action( 'wp_footer', 'mycustom_wp_footer' );
 
function mycustom_wp_footer() {
?>
    <script type="text/javascript">
        document.addEventListener('wpcf7mailsent', function(event) {

            
            
        }, false);
    </script>
    <?php
}

// автообновление версии файлов

function enqueue_versioned_script( $handle, $src = false, $deps = array(), $in_footer = false ) {
	wp_enqueue_script( $handle, get_template_directory_uri() . $src, $deps, filemtime( get_template_directory() . $src ), $in_footer );
}

function enqueue_versioned_style( $handle, $src = false, $deps = array(), $media = 'all' ) {
	wp_enqueue_style( $handle, get_template_directory_uri() . $src, $deps = array(), filemtime( get_template_directory() . $src ), $media );
}

function themename_scripts() {
	enqueue_versioned_style( 'my-theme-style', $theme_uri.'/dest/css/production.min.css' );
	enqueue_versioned_script( 'my-theme-script', $theme_uri.'/dest/js/production.min.js', array( 'jquery'), true );
}
 
add_action( 'wp_enqueue_scripts', 'themename_scripts' );