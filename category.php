<?php get_header(); ?>
<section id="category" class="section-margin-top">
    <div class="dis-flex flex-wrap-wrap justify-content-center">
        <div class="col-lg-11">
            <div class="category-title">
                <h1>news</h1>
            </div>
        </div>
        <div class="col-lg-6 dis-grid grid-col-lg-2 grid-col-xs-1 grid-row-20">
            <?php if(have_posts()) : ?>
            <?php while(have_posts()) : the_post(); ?>
            <article class="category-post">
                <div class="category-post-image">
                    <?php the_post_thumbnail(); ?>
                </div>
                <div class="category-post-desc text-block">
                    <h3><?php the_title(); ?></h3>
                    <?php the_excerpt(); ?>
                </div>
            </article>
            <?php endwhile; ?>
            <?php endif; ?>
        </div>
    </div>
</section>
<?php get_footer(); ?>